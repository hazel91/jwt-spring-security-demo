package com.tanwb.excepiton;


import com.tanwb.enums.ExceptionEnum;

public class ParamException extends BasicException {
    public ParamException(ExceptionEnum exceptionEnum) {
        super(exceptionEnum);
    }
}
