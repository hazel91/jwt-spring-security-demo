package com.tanwb.excepiton;


import com.tanwb.enums.ExceptionEnum;

public class NotLoginException extends BasicException {
    public NotLoginException(ExceptionEnum exceptionEnum) {
        super(exceptionEnum);
    }
}
