package com.tanwb.repository;

import com.tanwb.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

public interface PermissionRepository extends JpaRepository<Permission, Long> {
    List<Permission> findByPermissionIdIn(Set<Long> permissionIdList);
}
